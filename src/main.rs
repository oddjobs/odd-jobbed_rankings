use anyhow::{Result, anyhow};
use futures::future::try_join_all;
use reqwest::Url;
use serde::Deserialize;
use std::{cmp::Ordering, io::Write, sync::Arc};
use tokio::sync::Mutex;

static PREAMBLE: &str = r##"# Unofficial “odd-jobbed rankings”

This “““rankings””” is almost certainly riddled with errors, inaccuracies, &amp;
missing information, &amp; should be treated as such. This is _purely_ for
informal use &mdash; so please don’t take it too seriously. If you’ve any
questions or doubts about what appears or doesn’t appear on this “rankings”,
then the answer is that this document originated solely as a _roster_, meaning
that its only utility is to see at a glance what odd-jobbers are actively (or,
at the very least, were known to be actively) playing on MapleLegends. Also
bear in mind that this document is maintained by one person, with limited
experience, knowledge, &amp; resources.

The levels (&amp; <span style="font-variant-caps: all-small-caps;">EXP</span>s)
of the characters listed
here are fetched directly from [the official MapleLegends web API
endpoint](https://maplelegends.com/api/) via [a Rust
script](https://codeberg.org/oddjobs/odd-jobbed_rankings/src/branch/master/src/main.rs).

To make the “rankings” actually maintainable, off-island characters who’ve
yet to achieve level 45, islanders who’ve yet to achieve level 40, &amp;
campers who’ve yet to achieve level 10 aren’t represented here.

<abbr title="in-game name"><dfn>IGN</dfn></abbr> stands for “in-game name”. The
<dfn>name</dfn> entries are mostly for discerning
when two or more characters are controlled by the same player. The names are
done on a best-effort basis, &amp; some of them are just
[Discord&trade;](https://en.wikipedia.org/wiki/Discord) identifiers
(which, it should be noted, can be changed at more or less any time, for any
reason).

Unknown or uncertain information is denoted by a question mark (“?”).

At the time of this writing, the alliance that the <b>Oddjobs</b> guild is a
member of &mdash; <b>Suboptimal</b> &mdash; also consists of the guilds
<b>Flow</b>, <b>lronman</b>, <b>Victoria</b>, &amp; <b>Ossyrians</b>. However,
this rankings is for MapleLegends in general, &amp; so represents many
characters who aren’t Suboptimal members.

For more information on what constitutes an “odd job” for the purpose of this
here arbitrary listing, see
[here](https://deer.codeberg.page/diary/098/#perma-2nd-jobness-may-be-odd-on-its-own),
&amp;
[especially here](https://deer.codeberg.page/diary/114/).

| <abbr title="in-game name">IGN</abbr> | name | level | job(s) | guild |
| :--------- | :----------- | ----: | :---------------- | ------------- |
"##;

#[derive(Deserialize)]
struct CharsJson {
    chars: Vec<Char>,
}

#[derive(Deserialize)]
struct Char {
    // In chars.json:
    ign: String,
    name: Option<String>,
    job: String,

    // Fetched:
    level: Option<u8>,
    exp_percent: Option<f32>,
    guild: Option<String>,
}

#[derive(Deserialize)]
struct LegendsApiResponse {
    guild: String,
    //name: String,
    level: u8,
    //job: String,
    exp: String,
    //quests: u16,
    //cards: u16,
    //donor: bool,
    //fame: i32,
}

#[tokio::main]
async fn main() -> Result<()> {
    // Intentionally blocking here.
    let chars_json: CharsJson =
        serde_json::from_reader(std::fs::File::open("chars.json")?)?;
    let char_count = chars_json.chars.len();
    let chars = Arc::new(Mutex::new(chars_json.chars));

    // The actual async part.
    let client = reqwest::Client::new();
    for res in try_join_all((0..char_count).map(|i| {
        tokio::spawn(fetch_info(client.clone(), Arc::clone(&chars), i))
    }))
    .await?
    {
        res?;
    }

    // More intentional blocking.
    let mut output_file = std::fs::File::create("README.md.temp")?;
    output_file.write_all(PREAMBLE.as_bytes())?;
    let mut cs = chars.lock().await;
    cs.sort_unstable_by(|c1, c2| match (c1.level, c2.level) {
        (Some(l1), Some(l2)) => {
            l2.cmp(&l1).then(match (c1.exp_percent, c2.exp_percent) {
                (Some(ep1), Some(ep2)) => ep2.total_cmp(&ep1),
                _ => Ordering::Equal,
            })
        }
        _ => Ordering::Equal,
    });
    for c in cs.iter() {
        let name_buf;
        let name = if let Some(name) = &c.name {
            name_buf = markdown_esc(name);

            &name_buf
        } else {
            "?"
        };
        let guild_buf;
        let guild = if let Some(guild) = &c.guild {
            guild_buf = markdown_esc(guild); // Excessively paranoid, I know.

            &guild_buf
        } else {
            r"&lsqb;<i>none</i>\]"
        };

        writeln!(
            &mut output_file,
            "| {} | {name} | {} | {} | {guild} |",
            markdown_esc(&c.ign), // Excessively paranoid, I know.
            c.level.ok_or_else(|| anyhow!(
                "No level available for IGN {}",
                c.ign,
            ))?,
            markdown_esc(&c.job),
        )?;
    }

    output_file.flush()?;
    std::fs::rename("README.md.temp", "README.md")?;

    Ok(())
}

async fn fetch_info(
    client: reqwest::Client,
    chars: Arc<Mutex<Vec<Char>>>,
    char_ix: usize,
) -> Result<()> {
    let url = {
        let mut url = String::with_capacity(
            "https://maplelegends.com/api/character?name=".len() + 12,
        );
        url.push_str("https://maplelegends.com/api/character?name=");
        url.push_str(&chars.lock().await[char_ix].ign);

        Url::parse(&url)?
    };
    let resp = client
        .get(url.clone())
        .send()
        .await
        .map_err(|e| e.with_url(url.clone()))?
        .json::<LegendsApiResponse>()
        .await
        .map_err(|e| e.with_url(url))?;

    let mut cs = chars.lock().await;
    let c = &mut cs[char_ix];
    c.level.replace(resp.level);
    c.exp_percent = resp
        .exp
        .get(..resp.exp.len() - 1)
        .and_then(|s| s.parse().ok());
    c.guild = if resp.guild.is_empty() {
        None
    } else {
        Some(resp.guild)
    };

    Ok(())
}

fn markdown_esc(s: &str) -> String {
    let mut escaped = String::with_capacity(s.len());
    for c in s.chars() {
        if c == '[' {
            escaped.push_str("&lsqb;");
        } else {
            match c {
                '&' | '.' | '_' | '*' | '+' | '-' | '=' | '(' | ')' | '['
                | ']' | '{' | '}' | '<' | '>' | '#' | '~' | '^' | '\\'
                | '`' | '|' | '!' => escaped.push('\\'),
                _ => (),
            }
            escaped.push(c);
        }
    }

    escaped
}
