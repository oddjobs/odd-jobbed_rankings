# Unofficial “odd-jobbed rankings”

This “““rankings””” is almost certainly riddled with errors, inaccuracies, &amp;
missing information, &amp; should be treated as such. This is _purely_ for
informal use &mdash; so please don’t take it too seriously. If you’ve any
questions or doubts about what appears or doesn’t appear on this “rankings”,
then the answer is that this document originated solely as a _roster_, meaning
that its only utility is to see at a glance what odd-jobbers are actively (or,
at the very least, were known to be actively) playing on MapleLegends. Also
bear in mind that this document is maintained by one person, with limited
experience, knowledge, &amp; resources.

The levels (&amp; <span style="font-variant-caps: all-small-caps;">EXP</span>s)
of the characters listed
here are fetched directly from [the official MapleLegends web API
endpoint](https://maplelegends.com/api/) via [a Rust
script](https://codeberg.org/oddjobs/odd-jobbed_rankings/src/branch/master/src/main.rs).

To make the “rankings” actually maintainable, off-island characters who’ve
yet to achieve level 45, islanders who’ve yet to achieve level 40, &amp;
campers who’ve yet to achieve level 10 aren’t represented here.

<abbr title="in-game name"><dfn>IGN</dfn></abbr> stands for “in-game name”. The
<dfn>name</dfn> entries are mostly for discerning
when two or more characters are controlled by the same player. The names are
done on a best-effort basis, &amp; some of them are just
[Discord&trade;](https://en.wikipedia.org/wiki/Discord) identifiers
(which, it should be noted, can be changed at more or less any time, for any
reason).

Unknown or uncertain information is denoted by a question mark (“?”).

At the time of this writing, the alliance that the <b>Oddjobs</b> guild is a
member of &mdash; <b>Suboptimal</b> &mdash; also consists of the guilds
<b>Flow</b>, <b>lronman</b>, <b>Victoria</b>, &amp; <b>Ossyrians</b>. However,
this rankings is for MapleLegends in general, &amp; so represents many
characters who aren’t Suboptimal members.

For more information on what constitutes an “odd job” for the purpose of this
here arbitrary listing, see
[here](https://deer.codeberg.page/diary/098/#perma-2nd-jobness-may-be-odd-on-its-own),
&amp;
[especially here](https://deer.codeberg.page/diary/114/).

| <abbr title="in-game name">IGN</abbr> | name | level | job(s) | guild |
| :--------- | :----------- | ----: | :---------------- | ------------- |
| rusa | deer | 187 | darksterity knight | Oddjobs |
| SwordFurbs | Sword Furb | 178 | DEXadin | Oddjobs |
| tarandus | deer | 176 | shield pugilist \(buccaneer\) | Oddjobs |
| Without | Elie | 171 | STR/DEX hybridginner | Pariah |
| LawdHeComin | JunkyardBat | 163 | F/P archgish | Oddjobs |
| AppleBasket | JunkyardBat | 151 | bishletish | Oddjobs |
| capreolina | deer | 150 | woodsmaster | Oddjobs |
| Battlesage | Red | 146 | F/P archgish | Oddjobs |
| Taima | Kelsey | 145 | STRginner | Oddjobs |
| Yoshis | Sword Furb | 139 | swashbuckler | Oddjobs |
| alces | deer | 136 | daggerlord | Oddjobs |
| Otios | Wout | 136 | STRginner | Foreign |
| cervid | deer | 133 | STR bishop | Oddjobs |
| daggerknight | Jonathan | 132 | dagger dark knight | Oddjobs |
| cervine | deer | 128 | I/L archmagelet | Oddjobs |
| Sunken | SunkenSpore | 127 | gishop | Oddjobs |
| Tacgnol | Kelsey | 127 | F/P archgishlet | Oddjobs |
| TooIs | SoftBoiled | 125 | bishlet | Oddjobs |
| Cassandro | Marcelo | 124 | claw bishlet | Oddjobs |
| Jestterz | Jestterz | 123 | STRginner | Oddjobs |
| eject | Caleb | 121 | DEXadin | Oddjobs |
| oddlesseW | lesseW | 120 | daggerlord | Oddjobs |
| GTOPDEFENDER | rogueinvogue | 120 | STR F/P archmage | Oddjobs |
| Permanovice | Red | 120 | STRginner | Oddjobs |
| inject | Caleb | 120 | dagger hero | Oddjobs |
| Fabiennes | Sword Furb | 112 | F/P gish | Oddjobs |
| Gumby | Jonathan | 110 | STRginner | Flow |
| Furbs | Sword Furb | 109 | STRginner | Oddjobs |
| Johnny | Jonathan | 107 | daggermit | Reboot |
| Daddyo | Hunter | 107 | STRginner | &lsqb;<i>none</i>\] |
| Keppet | Rapskal | 105 | daggermit | Oddjobs |
| d34r | deer | 103 | dagger spearwoman \(Vicloc\) | Victoria |
| Sakuzyo | NoChorus | 103 | pugilist \(marauder\) | Oddjobs |
| axisaxis | deer | 102 | LUK WK | &lsqb;<i>none</i>\] |
| OmokTeacher | Noam | 102 | STRginner | Flow |
| ducklings | Joyce | 100 | STRginner | DuckNation |
| ImSadAllDay | ? | 100 | STRginner | Oddjobs |
| Outside | Tab | 100 | STRginner | Flow |
| Boymoder | Kelsey | 98 | STRmit | Oddjobs |
| Sn0wDr0p | ? | 97 | dagger dragon knight \(PPQ mule\) | &lsqb;<i>none</i>\] |
| Ananda | Red | 97 | gish priest | Oddjobs |
| xX17Xx | mae | 97 | permarogue | Oddjobs |
| Cortical | cort | 96 | STRginner | Flow |
| hydropotina | deer | 94 | swashbuckler | Oddjobs |
| Hanyou | Kelsey | 93 | DEX WK | Oddjobs |
| Medulla | cort | 92 | dagger sader | Oddjobs |
| rogueinvogue | rogueinvogue | 91 | permarogue | Oddjobs |
| GishGallop | cort | 89 | I/L gish | Oddjobs |
| Aniket | Red | 89 | woodsman \(sniper\) | &lsqb;<i>none</i>\] |
| Tiffany | Jonathan | 88 | DEX marauder | Oddjobs |
| MeikoHonma | Meiko | 87 | STRginner | Renaissance |
| Newbton | Jonte | 87 | islander | Aoshima |
| Cowbelle | Belle | 87 | STRginner | Homies |
| panolia | deer | 87 | permarogue | Oddjobs |
| pleLeg | JunkyardBat | 87 | hybrid DEX/STR sader | Oddjobs |
| inhale | Caleb | 86 | DEXginner | Oddjobs |
| Dakota | less | 86 | cowgirl | Oddjobs |
| gogigagagigo | boop | 86 | permarcher | Oddjobs |
| Ismezin | João | 85 | STRginner | Flow |
| uayua | cecilia | 84 | STRginner | Oddjobs |
| jung1e | Justin | 84 | islander | avalon |
| HasteWhacker | ? | 83 | STRmit | Oddjobs |
| Swathelby | Swathelby | 82 | STRginner | Flow |
| shadowban | cecilia | 82 | LUKless chief dit | Oddjobs |
| Copo | Marcelo | 81 | permapirate | Oddjobs |
| d00r | deer | 81 | priestlet | Oddjobs |
| WhyDaggers | PhysicistJason | 80 | dagger sader | Oddjobs |
| Pulzar | ? | 78 | I/L magelet | Oddjobs |
| Duhm | Duhm | 77 | STRginner | Flow |
| rangifer | deer | 76 | pugilist \(marauder\) | &lsqb;<i>none</i>\] |
| Meipuru | ? | 76 | shield pugilist \(brawler\) | Oddjobs |
| Leekree | ? | 74 | STRginner | Oddjobs |
| Zheedie | David | 74 | islander | Aoshima |
| Phoneme | cort | 74 | permamagician | Victoria |
| Celim | Marcelo | 74 | STRginner | Flow |
| Christopher | Chris | 73 | islander | Aoshima |
| d33r | deer | 73 | clericlet \(Vicloc\) | Victoria |
| LiedOnJobApp | ? | 73 | STR cleric | Oddjobs |
| mase | fumumu | 72 | islander | locals |
| drainer | mae | 72 | STRginner | Flow |
| Edward | Jonathan | 72 | LUK WK | Oddjobs |
| ZeroSTR | chen\_95 | 72 | DEX dagger WK | Oddjobs |
| 1sme | João | 72 | islander | Oddjobs |
| tb303 | cecilia | 71 | gish priest | Flow |
| Nyanners | Kelsey | 71 | permawarrior \(steelwoman\) | Oddjobs |
| Ever | Becca | 70 | islander | Team |
| Wouty | Wout | 70 | permapirate | Foreign |
| elaphus | deer | 70 | DEX sader | Lesbian |
| MegaHeals | ? | 70 | STR priest | Oddjobs |
| Dargos | MaxMartin | 70 | dagger sader | Oddjobs |
| ratty | fumumu | 70 | permapirate | Foreign |
| LoveYouToo | ? | 70 | STRginner | &lsqb;<i>none</i>\] |
| NubKargo | Shane | 67 | besinner | Flow |
| KitsunyChang | Hannah | 66 | islander | Amherst |
| justbegin | Kalezus | 66 | STRginner | &lsqb;<i>none</i>\] |
| beaf | fumumu | 66 | punch slinger | Foreign |
| MapleStarter | Dvd | 66 | islander | avalon |
| KingSylvius | Adam | 66 | STRginner | Oddjobs |
| Furca | Gusing | 65 | clawslinger | Oddjobs |
| S3DAT3 | Wout | 65 | punch slinger | Foreign |
| Yishmo | ? | 65 | STRginner | Oddjobs |
| BeginnersEnd | Ben | 65 | STRginner | Flow |
| SnailDEX | choob | 64 | islander | &lsqb;<i>none</i>\] |
| AmazieDaisy | incorrigiblefool | 64 | woodswoman \(crossbow\) | Oddjobs |
| Contagion | Elie | 63 | islander | Amherst |
| Beginner626 | ? | 63 | islander | Southperry |
| Oahu | Tom | 63 | STRlander | Aoshima |
| FooIs | SoftBoiled | 62 | STR sin | Oddjobs |
| Wout | Wout | 61 | islander | locals |
| illadvised | Red | 61 | STRginner \(arealocked\) | Oddjobs |
| poggered | ? | 61 | STRginner | Oddjobs |
| doiob | doiob | 60 | STR cleric | Oddjobs |
| MapIeIsIand | ? | 60 | islander | &lsqb;<i>none</i>\] |
| LazySoup | ? | 60 | islander | Southperry |
| Godswood | eriklopez95 | 59 | STR cleric | Oddjobs |
| HPdit | RyeBread | 59 | blood bandit | &lsqb;<i>none</i>\] |
| PermaBGinR | ? | 58 | STRginner | Oddjobs |
| Cleft | Tom | 58 | STRginner | Flow |
| WrongPage | \.HP\. Fiend | 58 | dagger page | Oddjobs |
| Bloodit | ? | 57 | blood bandit | Oddjobs |
| Philippe | ? | 57 | STRginner | Oddjobs |
| GooderNoddy | TotallyNotShoe | 57 | DEF/HP STRginner | Oddjobs |
| Rort | Lin | 57 | STRginner | BuccGang |
| L0neW0lf16OO | Jestterz | 56 | besinner | Oddjobs |
| Fraye | ? | 56 | islander | Southperry |
| NoJobNoRules | Jared | 56 | islander | Southperry |
| JanitorPedro | Pedro\_ | 56 | STRginner | Flow |
| HPTsu | Tsu | 56 | HP fighter | Oddjobs |
| attackattack | mae | 56 | DEX page | Oddjobs |
| Slimusaurus | Noam | 55 | besinner | &lsqb;<i>none</i>\] |
| BeginnerKoek | KapiteinKoek | 55 | STRginner | Oddjobs |
| EasyIsland | ? | 55 | islander | Southperry |
| DexBlade | Caleb | 55 | DEX fighter | &lsqb;<i>none</i>\] |
| Suzuran | Calendula | 55 | blood bandit | Oddjobs |
| hashishi | deer | 54 | besinner | Oddjobs |
| Timberly | Tim | 53 | islander | Southperry |
| VillageIdiot | ? | 53 | islander | Southperry |
| bricklayer | Marcelo | 53 | brigand | Oddjobs |
| Shikatanai | ? | 53 | islander | &lsqb;<i>none</i>\] |
| Numidium | Kelsey | 53 | STR cleric | Oddjobs |
| OrangeFungus | JunkyardBat | 52 | woodsman \(crossbow\) | Oddjobs |
| SwashKoekler | KapiteinKoek | 52 | swashbuckler | Oddjobs |
| Comes | Noam | 52 | swashbuckler | Oddjobs |
| Bertolt | Jonathan | 52 | summoner | Oddjobs |
| Bushroom | robidouux | 52 | F/P wizardlet | Oddjobs |
| LightGuard | felyp244 | 52 | DEX page | Oddjobs |
| Desolate | Logan | 51 | islander | Amherst |
| PinkCream | Robin | 51 | islander | Aoshima |
| KoKoSnuggles | Snuggles | 51 | islander | Southperry |
| Ashore | Sean | 51 | islander | Aoshima |
| Sommer | Marcelo | 51 | swashbuckler | Oddjobs |
| Neveroffland | ? | 51 | islander | avalon |
| Luckiestboy | Dvd | 51 | LUKlander | avalon |
| HPdagger | Charles | 51 | dagger fighter | Oddjobs |
| hueso | cecilia | 51 | permapirate | Oddjobs |
| Zrar | PhysicistJason | 50 | islander | Southperry |
| ScrubDaddy | Scrub | 50 | STRginner | Oddjobs |
| insist | Caleb | 50 | swashbuckler | Oddjobs |
| Gambolpuddy | Kelsey | 50 | DEXginner | Oddjobs |
| Camomile | Vivian | 50 | islander | &lsqb;<i>none</i>\] |
| BowerStrike | cort | 50 | bow\-whacker | Oddjobs |
| sorts | deer | 50 | DEX brawler \(LPQ mule\) | Oddjobs |
| VigiI | Harley | 49 | I/L gish \(Vicloc\) | Victoria |
| hisGish | ? | 49 | I/L gish | Oddjobs |
| TheFalseKing | ? | 48 | islander | Southperry |
| kelik | ratz | 48 | STRginner | Oddjobs |
| AychPea | MrPringles | 48 | HP page | Oddjobs |
| DumbIsland | ? | 48 | islander | Southperry |
| NewIslander | ? | 48 | islander | &lsqb;<i>none</i>\] |
| Kimberly | Jonathan | 47 | islander | Flow |
| Leozinho | Leozinho | 47 | perfectlander | Oddjobs |
| Peep | ? | 47 | islander | &lsqb;<i>none</i>\] |
| ozotoceros | deer | 47 | DEX/LUK hybridlander | Oddjobs |
| Nabisco | idek | 47 | DEXdit | Oddjobs |
| FairRow | FairRow | 47 | STRginner | Oddjobs |
| Dexual | Hunter | 46 | islander | Southperry |
| CokeZeroPill | cort | 46 | gish cleric | Oddjobs |
| Brigandz | ? | 46 | brigand | Oddjobs |
| Femdom | ? | 46 | islander | Newbology |
| Mujintou | ? | 46 | islander | Pomf |
| Grenadier | Marcelo | 46 | grenadier | Oddjobs |
| SolidState | DarkCookie | 46 | DEXdit | Oddjobs |
| Hitodama | Kelsey | 45 | brigand | Oddjobs |
| Weakling | ? | 45 | brigand | Flow |
| PinkBean | ? | 45 | islander | Shroomies |
| IslandLiving | ? | 45 | islander | Southperry |
| c7gs | ratz | 45 | islander | Southperry |
| Fino | Marcelo | 45 | I/L gish | Oddjobs |
| Tsukishima | ? | 45 | islander | Southperry |
| WettestDog | Hebaio | 45 | islander | Southperry |
| nonk | ? | 45 | LUKless dit | Oddjobs |
| Mushers | JunkyardBat | 45 | lightning gish | Oddjobs |
| WaII | ? | 44 | islander | Cute |
| Babbi | Alvin | 44 | islander | Southperry |
| Steam | ? | 44 | islander | &lsqb;<i>none</i>\] |
| Vacation | ? | 44 | islander | Amherst |
| Ninnie | Ninnie | 44 | islander | Pomf |
| tbook | ? | 44 | islander | avalon |
| Gosu | ? | 43 | islander | &lsqb;<i>none</i>\] |
| SentientBlob | ? | 43 | islander | Southperry |
| 2015 | ? | 43 | islander | Oddjobs |
| o0Oo0ooOo0o0 | ? | 43 | islander | Amherst |
| Patrao | Duncan | 42 | islander | Southperry |
| Cottage | ? | 42 | islander | Southperry |
| Woogie | ? | 42 | islander | Southperry |
| Snapples | Kristina | 42 | islander | Newbology |
| Snuggy | ? | 41 | islander | Aoshima |
| Mesa | sneaks | 41 | islander | Southperry |
| BirdMandala | ? | 41 | islander | Southperry |
| Dreamscapes | Red | 41 | perfectlander | GangGang |
| Subcortical | cort | 41 | islander | Southperry |
| Zugar | ? | 41 | islander | Newbology |
| Retain | ? | 40 | islander | Aoshima |
| SlimeySlayer | ? | 40 | islander | Southperry |
| Dannyslsland | ? | 40 | islander | Southperry |
| Kokoro | ? | 40 | islander | &lsqb;<i>none</i>\] |
| iamDanix | ? | 40 | islander | Southperry |
| LeafSpirit | ? | 40 | islander | Southperry |
| GoGoSnuggles | Snuggles | 30 | camper | Flow |
| Camperz | Zheedie | 28 | camper | Aoshima |
| WarmApplePie | Snuggles | 27 | camper | &lsqb;<i>none</i>\] |
| Mirelly | ? | 24 | camper | Oddjobs |
| Itati | ? | 19 | camper | &lsqb;<i>none</i>\] |
| Tutorialist | ? | 17 | camper | Amherst |
| BloodyTaco | ? | 16 | camper | &lsqb;<i>none</i>\] |
| Hyewon | ? | 15 | camper | &lsqb;<i>none</i>\] |
| DuckyWucky | ? | 15 | camper | &lsqb;<i>none</i>\] |
| Hobbits | ? | 14 | camper | &lsqb;<i>none</i>\] |
| NewCharacter | cort | 13 | camper | Flow |
| 1EXP | ? | 13 | camper | &lsqb;<i>none</i>\] |
| Greening | ? | 12 | camper | &lsqb;<i>none</i>\] |
| Mokoko | ? | 12 | camper | &lsqb;<i>none</i>\] |
| TrainingCamp | ? | 12 | camper | Campers |
| Kept | ? | 11 | camper | Campers |
| Chronosius | ? | 11 | camper | &lsqb;<i>none</i>\] |
| Words | ? | 10 | camper | &lsqb;<i>none</i>\] |
| Social | ? | 10 | camper | &lsqb;<i>none</i>\] |
| midorin | Kelsey | 10 | camper | Oddjobs |
